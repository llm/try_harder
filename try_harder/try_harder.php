<?php

/*function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}*/

/*
 * SPECIFIC PAGES
 */

/**
 * Hook render_linklist.
 *
 * Template placeholders:
 *   - action_plugin: next to 'private only' button.
 *   - plugin_start_zone: page start
 *   - plugin_end_zone: page end
 *   - link_plugin: icons below each links.
 *
 * Data:
 *   - _LOGGEDIN_: true/false
 *
 * @param array $data data passed to plugin
 *
 * @return array altered $data.
 */
function hook_try_harder_render_linklist($data)
{
    //$archive_html = file_get_contents(PluginManager::$PLUGINS_PATH . '/archiveorg/archiveorg.html');
    $try_harder_html = file_get_contents(PluginManager::$PLUGINS_PATH . '/try_harder/field.html');
	$websites = array('http://lehollandaisvolant.net','http://shaarli.callmematthi.eu');
    foreach ($data['links'] as &$value) {
        $url = $value['real_url'];
        $found = false;
        foreach ($websites as $website)
        {
			if (!$found)
			{
				$found = startsWith($url,$website,false);
			}
		}
		if ($found)
		{
			$url = 'http://anonym.to/?'.$url;
		}
        $value['real_url'] = $url;
        
        $broken = sprintf($try_harder_html, $value['url']);
        $value['link_plugin'][] = $broken;
    }

    return $data;
}

/**
 * Execute render_feed hook.
 * Called with ATOM and RSS feed.
 *
 * Special data keys:
 *   - _PAGE_: current page
 *   - _LOGGEDIN_: true/false
 *
 * @param array $data data passed to plugin
 *
 * @return array altered $data.
 */
function hook_try_harder_render_feed($data)
{
    /*foreach ($data['links'] as &$link) {
        if ($data['_PAGE_'] == Router::$PAGE_FEED_ATOM) {
            $link['description'] .= ' - ATOM Feed' ;
        }
        elseif ($data['_PAGE_'] == Router::$PAGE_FEED_RSS) {
            $link['description'] .= ' - RSS Feed';
        }
    }*/
    return $data;
}
